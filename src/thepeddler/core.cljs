(ns thepeddler.core
  (:require-macros [cljs.core.async.macros :refer [go go-loop]]
                   [clojure.string :as string])
  (:require [hipo.core :as hipo]
            [garden.core :refer [style]]
            [clojure.string :as str]
            [goog.dom :as dom]
            [goog.events :as events]
            [goog.string :as gstring]
            [goog.string.format]))

(defonce DEBUG false)

(enable-console-print!)

(defonce app-state (atom
                    {:mouse-x 0
                     :mouse-y 0
                     :mouse-down false
                     :held nil
                     :offered nil
                     :peddler-modifier 1
                     :drop-rate 2
                     :junk-rate 3
                     :visible-pages ["Peddler's" "hand"]
                     :page-order ["Peddler's"]
                     :dialog []
                     :gold 10000
                     :total 10000
                     :upgrades #{}
                     :new-bag-size-x 14
                     :new-bag-size-y 6
                     :hero-portrait-a "/images/Hero/HeroS1.png"
                     :hero-portrait-b "/images/Hero/HeroS1.png"
                     :peddler-portrait "/images/Peddler.png"
                     :pages {"Peddler's" {:size-x 18 :size-y 6}
                             "hand" {:size-x 3 :size-y 5}}}))

;; STATE HELPERS

(defn listed [hash]
  (map #(assoc (get hash %) :id %) (keys hash)))

;;DOM EVENT HANDLERS

(defn apply-state [event & [args]]
  (swap! app-state event args))

(defn get-page-in [state page]
  (get-in state [:pages page]))

(defn get-page [page]
  (get-page-in @app-state page))

(defn annotate-page [page-id shits]
  (if-not (nil? shits)
    (map #(assoc % :page page-id) (listed shits))))

(defn get-shits-in [state]
  (mapcat
   #(annotate-page % (:shits (get-page-in state %)))
   (:visible-pages state)))

(defn get-shits []
  (get-shits-in @app-state))

(defn mount-key [shit]
  (str (:page shit) "-" (:mount-x shit) "-" (:mount-y shit)))

(defn update-mount [id page x y]
  (swap! app-state
         #(let [shit (first (filter (fn [v] (= (:id v) id)) (get-shits)))]
            (-> %
                (update-in [:pages (:page shit) :shits] dissoc id)
                (assoc-in [:pages page :shits id] shit)
                (assoc-in [:pages page :shits id :mount-x] x)
                (assoc-in [:pages page :shits id :mount-y] y)))))

(defn hold [s id]
  (assoc s :held id))

(defn stop-holding [s]
  (assoc s :held nil))

;;DOM EVENT HANDLER)S

(declare element)
(defn update-client-size [old event]
  (let [app-box (.getBoundingClientRect (element "app"))]
    (-> old
        (assoc :screen-x (.-width app-box))
        (assoc :screen-y (.-height app-box)))))

(defn update-mouse-position [old event]
  (-> old
      (assoc :mouse-x (.-clientX event))
      (assoc :mouse-y (.-clientY event))))

(defonce mouse-position-handler
  (events/listen js/document "mousemove"
                 #(apply-state update-mouse-position %)))

(defonce mouse-down-handler
  (events/listen js/document "mousedown"
                 #(swap! app-state (fn [s] (assoc s :mouse-down true)))))

(defn mouse-up [s] (assoc s :mouse-down false))

(defonce mouse-up-handler
  (events/listen js/document "mouseup"
                 #(apply-state (comp stop-holding mouse-up))))

(defonce window-resize-handler
  (events/listen js/window "resize" #(swap! app-state update-client-size)))

(defonce window-onload-handler
  (events/listen js/window "load" #(swap! app-state update-client-size)))

;; DOM HELPERS

(def neg (partial * -1))
  (def bg-color "rgba(0,44,0,0.8)")

(def fg-color "rgba(250,250,250,1.0)")

(def inv-border "3px solid rgba(131,104,61,1)")

(defn active-hero []
  (if (.contains (.-classList (element "hero-b")) "hero-enter")
    "hero-a"
    "hero-b"))

(defn hero-enter [hero]
  (doto (.-classList hero)
    (.remove "hero-leave")))

(defn hero-here [hero]
  (doto (.-classList hero)
    (.remove "hero-enter")
    (.remove "hero-leave")))

(defn hero-leave [hero]
  (doto (.-classList hero)
    (.add    "hero-enter")
    (.add    "hero-leave")))

(defn hero-toggle []
  (let [hero-a (element "hero-a")
        hero-b (element "hero-b")]
    (if (= (active-hero) "hero-b")
      (do
        (hero-here hero-a)
        (hero-leave hero-b)
        (.setTimeout js/window #(hero-enter hero-b) 1000))
      (do
        (hero-here hero-b)
        (hero-leave hero-a)
        (.setTimeout js/window #(hero-enter hero-a) 1000)))))

(defn vw-slow [n]
  (str n "vw"))
(def vw (memoize vw-slow))

(defn px-slow [n]
  (str n "px"))

(defn px [n]
  ((memoize px-slow) n))

(defn padding []
  (px (/ (min (:screen-x @app-state) (:screen-y @app-state)) 100)))

(defn margin
  ([]
   (px (/ (min (:screen-x @app-state) (:screen-y @app-state)) 80)))
  ([n]
   (* n (margin))))

(defn css-url [s]
  (str "url(" s ")"))

(defn s [b & [extra]]
  (let [styled (memoize style)]
             (merge {:style (styled b)} extra)))

(defonce body-element
 (aget (.getElementsByTagName js/document "BODY") 0))

(def element (fn [id]
  (.getElementById js/document id)))

(defn play-coins []
  (.play (element (str "coins" (+ 1 (rand-int 5))))))

(defn inside [held target page]
  (let [left (int (:x target))
        right  (+ left (int (:size-x held)))
        top (int (:y target))
        bottom (+ top (int (:size-y held)))]
    (and (>= top 0)
         (<= bottom (:size-y page))
         (<= right (:size-x page))
         (>= left 0))))

(defn intersects [held target other]
  (let [other-left (int (:mount-x other))
        other-right (+ other-left (int (:size-x other)))
        other-top (int (:mount-y other))
        other-bottom (+ other-top (int (:size-y other)))
        left (int (:x target))
        right (+ left (int (:size-x held)))
        top (int (:y target))
        bottom (+ top (int (:size-y held)))
        result (and (not (= (:id held) (:id other)))
                    (not (or (>= top other-bottom)
                             (<= right other-left)
                             (<= bottom other-top)
                             (>= left other-right))))]
        result))

(defn extract-values [e]
  {:x (int (.getAttribute e "x"))
   :y (int (.getAttribute e "y"))
   :id (int (.getAttribute e "id"))
   :size-x (int (.getAttribute e "size-x"))
   :size-y (int (.getAttribute e "size-y"))})

(defn shit-name [shit]
  (str (:prefix shit)
       (if (:prefix shit) "'s ")
       (:root shit)
       (if (:suffix shit) " of ")
       (:suffix shit)))

(defn valid-placement [held target page]
  (and (inside held target page)
       (nil? (some #(intersects held target %) (listed (:shits page))))
       (if (= page (get-page "hand")) (= (count (:shits page)) 0) true)))

(defn rank [type shit hero]
  (+ 1 (.indexOf (get (:preferences hero) type) (get shit type))))

(defn modifier [type shit hero]
  (if (type shit) (* 5 (js/Math.pow (/ 1 (rank type shit hero)) 1.8))))

(defn shift [type shit hero]
  (if (type shit) (if (< (rank type shit hero) 3) 100)))

(declare POTION)
(defn calculate-value
  ([shit] (or (:base-value shit) (:cost shit)))
  ([shit hero]
   (if (nil? shit) nil
       (js/Math.floor
        (*
         (:modifier (first (vals (:stinginess hero))))
         (:peddler-modifier @app-state)
         (+ 1
              (modifier :prefix shit hero)
              (modifier :suffix shit hero)
              (if (= (:type shit) POTION) (* 2 (modifier :suffix shit hero)))
              (if (= (:type shit) POTION) (* 2 (modifier :root shit hero)))
              (modifier :root shit hero))
         (+ (shift :prefix shit hero)
            (shift :suffix shit hero)
            (shift :root shit hero)
            (:base-value shit)))))))

(declare MONSTER)
(defn calculate-offering [shit hero]
  (if (or (nil? shit) (= (:type shit) MONSTER)) nil
      {:prefix-rank (rank :prefix shit hero)
       :root-rank (rank :root shit hero)
       :suffix-rank (rank :suffix shit hero)
       :base-price (:base-value shit)
       :name (shit-name shit)
       :gold (calculate-value shit hero)}))

(defn check-price []
  (let [shit (first (listed (:shits (get-page "hand"))))]
      (apply-state #(assoc % :offered (calculate-offering shit (:hero @app-state))))))

(defn set-top-z [id]
  (let [target (element id)]
    (aset (-> target .-style) "z-index" "100")))

(defn restore-z [id]
  (let [target (element id)]
    (aset (-> target .-style) "z-index" "4")))

(defn search-grid [size-x size-y page-id]
  (flatten (map-indexed (fn [y xs] (map-indexed #(do {:x %1 :y y :page page-id}) xs))
                        (repeat size-y (repeat size-x '())))))

(defn find-valid-placement
  ([shit]
   (remove nil? (map (fn [page-id]
                       (let [page (get-in @app-state [:pages page-id])
                             search-space (search-grid (:size-x page) (:size-y page) page-id)
                             predicate #(if (valid-placement shit % page) % nil)]
                         (first (filter predicate search-space)))) (remove #(= "hand" %) (keys (:pages @app-state))))))
  ([shit page-id]
   (let [page (get-in @app-state [:pages page-id])
         search-space (search-grid (:size-x page) (:size-y page) page-id)
         predicate #(if (valid-placement shit % page) % nil)]
     (first (filter predicate search-space)))))

(defn released-in-grid [held target]
    (if (and held target)
      (if (valid-placement (merge (extract-values held) {:id (.-id held)})
                           (extract-values target)
                           (get-page (.getAttribute target "page")))
        (do (update-mount (.-id held)
                          (.getAttribute target "page")
                          (.getAttribute target "x")
                          (.getAttribute target "y"))
            (check-price)))))

(defn released-in-tab [name]
  (if-let [held (element (:held @app-state))]
    (let [valid (find-valid-placement (merge (extract-values held) {:id (:held @app-state)}) name)]
      (if-not (nil? valid)
        (update-mount (:held @app-state)
                          (:page valid)
                          (:x valid)
                          (:y valid))))))

;; Time to GENERATE

(defn always-pick [list]
  (nth list (rand-int (count list)) nil))

(defn pick [list]
  (if (< (rand-int 100) 70)
    (always-pick list) nil))

(def MELEE "Melee Weapon")
(def RANGED "Ranged Weapon")
(def ARMOR "Armor")
(def SHIELD "Shield")
(def MAGIC "Magic")
(def POTION "Potion")
(def MONSTER "Monster")

(def types-of-shit
  {
   "Arrow"
   {:type RANGED
    :size-x 1
    :size-y 1
    :base-value 2}
   "Arrows"
   {:type RANGED
    :size-x 2
    :size-y 1
    :base-value 60}
   "Battle Axe"
   {:type MELEE
    :size-x 2
    :size-y 3
    :base-value 442}
   "Boot"
   {:type ARMOR
    :size-x 2
    :size-y 2
    :base-value 214}
   "Bow"
   {:type RANGED
    :size-x 1
    :size-y 4
    :base-value 129}
   "Buckler"
   {:type SHIELD
    :size-x 2
    :size-y 2
    :base-value 93}
   "Club"
   {:type MELEE
    :size-x 1
    :size-y 3
    :base-value 21}
   "Greater Potion"
   {:type POTION
    :size-x 2
    :size-y 2
    :base-value 65}
   "Hat"
   {:type ARMOR
    :size-x 2
    :size-y 1
    :base-value 51}
   "Javelin"
   {:type RANGED
    :size-x 1
    :size-y 4
    :base-value 21}
   "Kite Shield"
   {:type SHIELD
    :size-x 2
    :size-y 3
    :base-value 172}
   "Knife"
   {:type MELEE
    :size-x 1
    :size-y 2
    :base-value 91}
   "Lesser Potion"
   {:type POTION
    :size-x 1
    :size-y 2
    :base-value 32}
   "Long Sword"
   {:type MELEE
    :size-x 1
    :size-y 4
    :base-value 143}
   "Minor Potion"
   {:type POTION
    :size-x 1
    :size-y 1
    :base-value 21}
   "Orb"
   {:type MAGIC
    :size-x 2
    :size-y 2
    :base-value 995}
   "Pants"
   {:type ARMOR
    :size-x 2
    :size-y 3
    :base-value 142}
   "Robe"
   {:type MAGIC
    :size-x 2
    :size-y 3
    :base-value 854}
   "Rocks"
   {:type RANGED
    :size-x 2
    :size-y 1
    :base-value 5}
   "Rock"
   {:type RANGED
    :size-x 1
    :size-y 1
    :base-value 1}
   "Tower Shield"
   {:type SHIELD
    :size-x 3
    :size-y 3
    :base-value 403}
  "Short Sword"
  {:type MELEE
   :size-x 2
   :size-y 3
   :base-value 122}
  "Spear"
  {:type MELEE
   :size-x 1
   :size-y 5
   :base-value 283}
  "Staff"
  {:type MAGIC
   :size-x 1
   :size-y 5
   :base-value 793}})

(def shit-prefixes
  {POTION []
   MELEE ["Paladin" "Knight" "Soldier" "Barbarian"
          "Cleric" "Enchanter" "Shaman" "Necromancer"
          "Rogue" "Marksman" "Bard" "Ninja"]
   ARMOR ["Paladin" "Knight" "Soldier" "Barbarian"
            "Cleric" "Enchanter" "Shaman" "Necromancer"
            "Rogue" "Marksman" "Bard" "Ninja"]
   RANGED ["Soldier" "Shaman" "Rogue" "Marksman" "Bard" "Ninja"]
   MAGIC ["Paladin" "Cleric" "Enchanter" "Shaman" "Necromancer" "Bard" "Ninja"]
   SHIELD ["Paladin" "Knight" "Soldier" "Barbarian" "Cleric"]})

(def shit-monsters ["Imp" "Wolf" "Tiger" "Bat" "Rat" "Pixie" "Slime"
                    "Centaur" "Crab" "Ogre" "Skeleton" "Bear" "Goblin"
                    "Beatle" "Dragon" "Spider"])

(declare has-upgrade)
(defn monster-bits []
  (merge (if (has-upgrade "Beetle Chow")
           {"Beetle Carapace" {:size-x 4 :size-y 3 :cost 500 :type MONSTER}})
         (if (has-upgrade "Giants In The Forest")
           {"Giant Toe" {:size-x 4 :size-y 6 :cost 1000 :type MONSTER}})
         {"Spider Eyes"     {:size-x 2 :size-y 1 :cost 100 :type MONSTER}
          "Slime Ball"      {:size-x 1 :size-y 1 :cost 200 :type MONSTER}
          "Snake Femur"     {:size-x 1 :size-y 2 :cost 300 :type MONSTER}
          "Bear Claw"       {:size-x 2 :size-y 2 :cost 400 :type MONSTER}
          "Pixie Dust"      {:size-x 5 :size-y 1 :cost 600 :type MONSTER}}))

(def slayings (map #(str % " Slaying") shit-monsters))

(def resistances (map #(str % " Resistance") shit-monsters))

(def shit-suffixes
  {POTION
   (concat slayings resistances
           (flatten (repeat 5
                            ["Restoration" "Remove Curse" "Remove Poison" "Resurrection"
                             "Inferno" "Lightning" "Flood" "'Fortitude" "Poison" "Curse"
                             "Protection" "Warding" "Smiting" "Summoning" "Overgrowth" "Might"
                             "Blindness" "Remove Blindness" "Web" "Healing"
                             "Mana" "Swiftness" "Regeneration"])))
   MELEE
   (concat slayings (flatten (repeat 10 ["Fortitude" "Restoration" "Swiftness" "Might"
                                         "Regeneration" "Accuracy" "Dexterity" "Luck"])))
   SHIELD
   (concat resistances (flatten (repeat 3 ["Fortitude" "Restoration" "Swiftness" "Might"
                                             "Regeneration" "Accuracy" "Dexterity" "Luck" "Protection"])))
   ARMOR
   (concat resistances (flatten (repeat 10 ["Fortitude" "Restoration" "Swiftness" "Might"
                                             "Regeneration" "Accuracy" "Dexterity" "Luck" "Protection"])))
   })

(defn a-an [string]
  (if (clojure.string/includes? "aeiouAEIOU" (first string))
    (str "an " string)
    (str "a " string)))

(defn shit-hint [shit]
  (let [part (always-pick [:root :suffix :suffix :suffix :root :prefix])
        clue (get shit part)]
    (case part
      :root (str (a-an clue))
      :prefix (str "something for " (a-an clue))
      :suffix (str "something of " clue))))

;; Stylish generator
(defn generate-shit []
  (let [root (always-pick (keys types-of-shit))
        base (get types-of-shit root)
        prefix (pick (get shit-prefixes (:type base)))
        suffix (pick (get shit-suffixes (:type base)))
        shit {:prefix prefix :suffix suffix :root root}]
    (merge base shit)))

(defn generate-monster-bit []
  (let [root (always-pick (keys (monster-bits)))
        base (get (monster-bits) root)]
    (merge base {:root root})))

(def all-prefixes
  (set (mapcat #(get shit-prefixes %) (keys shit-prefixes))))

(def all-suffixes
  (set (mapcat #(get shit-suffixes %) (keys shit-suffixes))))

(def all-types (keys types-of-shit))

(def weighted (comp conj repeat))

(def stinginesses (flatten (conj
                   (weighted 1 {"Unbelievably Trollish" {:modifier 0.1}})
                   (weighted 4 {"Selfish" {:modifier 0.2}})
                   (weighted 10 {"Jerk" {:modifier 0.5}})
                   (weighted 50 {"" {:modifier 1}})
                   (weighted 15 {"Nice" {:modifier 2}})
                   (weighted 6 {"Benevolent" {:modifier 5}})
                   (weighted 2 {"Patron Saint" {:modifier 10}}))))

(defn person [r c f rej] {:request r :continue c :final f :reject rej})

(def personalities
  (map #(apply person %)
       [
        ["Yo. Merchant man. Gimme %s"
         "Sweet. Gimme %s."
         "Thanks, man!"
         "Yo, bro. What'chu tryna pull? Pfft"]
        ["Excuse me, sir. I'm looking to buy %s."
         "Thank you. Do you also have %s"
         "A deal well struck. Good day to you."
         "Very well. I shall look elsewhere."]
        ["Yes, %s. That's what I want. Question is, do you have it?"
         "Excellent. What about %s?"
         "Have a good one!"
         "I guess not. See ya."]
        ["Umm, hullo. Erm, good day. Do you possibly happen to have %s?"
         "Mhm. Maybe, umm, %s too?"
         "You too. Erm, sorry. I meant, thank you. Bye."
         "Erm, sorry, I forgot that I already own that. Sorry. Bye."]
        ["Ho there, peddler of fine wares. I have traveled far and wide seeking %s. Only your best will do. I have not journeyed so long to find mediocrity."
         "Superb. The Fates smile down upon us. Mayhaps fortune will favor us further in my search for %s."
         "Thank you kindly, this is precisely what I sought"
         "Ah, I see. That is not quite what I had in mind. It seems this is not my journey's end after all. Good day to you."]
        ["If only I had %s, then maybe I could finish this quest line."
         "Yes, that's it! Hmm, I'm also going to need %s"
         "This is perfect. Thank you."
         "How am I supposed to earn fame and glory with that? I'll never become a true hero at this rate. Thanks for nothing."]
        ["Hey! I got a tip from another adventurer that you could sell me %s. Well, how about it?"
         "Swell! And %s?"
         "Wow, what a great deal! I'll be back, no doubt!"
         "Wow, that's insulting. I'll be posting a scathing review of your shop in the adventurer's guild hall."]
        ["Need %s, please. Make it snappy."
         "Fine. %s also."
         "That'll do, I suppose"
         "That's your offer? I suppose now we know why I'm the hero and you're the peddler. Good riddance."]
        ["You sure are a long ways out here. I wonder, do you have %s in stock?"
         "Yeah, that'll work. You have %s as well?"
         "Sure, I guess. After all, you are the only peddler around these parts."
         "Seriously? I'll take my business elsewhere."]
        ["Psst. Hey. You got %s?"
         "<i>nods surreptitiously and lingers.</i> Got %s?"
         "<i>nods surreptitiously</i>"
         "<i>squints skeptically</i> Tell no one I was here."]]
       ))

(def name-pnemes
  ["grab" "thar" "nor" "ns" "ber"
   "thor" "rem" "var" "as"
   "ateh" "mek" "daes" "med"
   "dottr" "son" "lyx" "aria"
   "cious" "el" "rond" "er" "ic"
   "dar" "fro" "do" "lego" "las"
   "gim" "ili" "lu" "ti" "dus"
   "roth" "sele" "mor" "dor" "gren" "del"
   "beo" "wolf" "lup" "pin" "daj" "jaam" "jinn"
   "ele" "phant" "gun" "dor" "guy" "brush" "threep" "wood"
   "thorn" "branch" "blood" "leaf" "stone" "rain" "water" "sea"
   "hel" "ga" "toma" "toa" "ma" "ui"]) 

(defn generate-name []
  (clojure.string/capitalize
   (apply str (repeatedly (+ 2 (rand-int 2)) #(always-pick name-pnemes)))))

(defn generate-hero []
  {:preferences
   {:prefix (shuffle all-prefixes)
    :suffix (shuffle all-suffixes)
    :root   (shuffle all-types)
    }
   :stinginess (always-pick stinginesses)
   :personality (always-pick personalities)
   :first-name (generate-name)
   :last-name (generate-name)
   })

(defn ideal-shit [hero]
  (let [root (-> hero (get-in [:preferences :root]) first)
        base (get types-of-shit root)
        prefix (-> hero (get-in [:preferences :prefix]) first)
        suffix (-> hero (get-in [:preferences :suffix]) first)]
    (merge base {:prefix prefix :suffix suffix :root root})))

(defn hero-name [hero]
  (let [stinginess (first (keys (:stinginess hero)))]
  (str (:first-name hero) " "
       (:last-name hero) " the "
       (if-not (empty? stinginess) (str stinginess " "))
       (first (:prefix (:preferences hero))))))

;; THE REMAINDER

(defn say [message]
  (apply-state (fn[r] (update r :dialog
                              #(conj % [:p (s {:margin-top (margin)
                                               :line-height "100%"
                                               :overflow "visible"
                                               :transform "rotate(-180deg)"})
                                        [:span (s {:color "gold"}) (str (-> r :hero :first-name) ": ")]
                                        message])))))

(defn log [message]
  (do (apply-state (fn[s] (update s :logs #(conj % message))))
      (set! (.-scrollTop (element "logs-list")) 0)))

(declare take-a-shit)
(declare take-a-bit)
(defn new-hero-appears [hero]
  (do
    (doall (repeatedly (+ 1 (rand-int (:drop-rate @app-state))) take-a-shit))
    (doall (repeatedly (+ 2 (rand-int (:junk-rate @app-state))) take-a-bit))
    (apply-state #(assoc % (if (= (active-hero) "hero-a") :hero-portrait-b :hero-portrait-a) (str "/images/Hero/Hero" (case (rand-int 3) 2 "D" 1 "S" 0 "G") (rand-int 14) ".png")))
    (hero-toggle)
    (say (gstring/format (get-in hero [:personality :request])
                         (shit-hint (ideal-shit hero))))))

(defn sell []
  (let [offering (:offered @app-state)]
    (if (nil? offering)
      (do
        (say (:reject (:personality (:hero @app-state))))
        (apply-state #(assoc % :hero (generate-hero)))
        (new-hero-appears (:hero @app-state)))
      (do
        (play-coins)
        (log (str "Sold " (:name (:offered @app-state))
                  " " (:gold (:offered @app-state)) " gp."))
        (apply-state #(-> %
                          (assoc :gold (+ (:gold %) (:gold (:offered %))))
                          (assoc :total (- (+ (:total %) (:gold (:offered %))) (:base-value (:offered %))))
                          (assoc :offered nil)
                          (assoc-in [:pages "hand" :shits] {})))
        (say (:final (:personality (:hero @app-state))))
        (apply-state #(assoc % :hero (generate-hero)))
        (new-hero-appears (:hero @app-state))))))

(defn expand-peddlers []
  (apply-state #(-> %
                            (update-in [:pages "Peddler's" :size-x] (comp inc inc))
                            (update-in [:pages "Peddler's" :size-y] inc))))

(defn expand-base-bag-size []
  (apply-state #(-> %
                    (update :new-bag-size-x (comp inc inc))
                    (update :new-bag-size-y inc))))

(defn improve-haggle []
  (apply-state (fn [state] (update state :peddler-modifier #(* 1.2 %)))))

(defn more-drops []
  (apply-state #(update % :drop-rate (comp inc inc))))

(defn more-junk []
  (apply-state #(update % :junk-rate (comp inc inc))))

(defn less-junk []
  (apply-state #(update % :junk-rate (comp dec dec dec))))

(def upgrades
  [
   {:id "Bigger pack"
    :description "Get a bigger pack"
    :price 2000
    :result "The meta-peddler has sold you a larger pack!"
    :on-buy expand-peddlers}
   {:id "Buy Bags"
    :description "Establish a relationship with the meta-peddler to purchase bags."
    :price 5000
    :result "The meta-peddler is a reclusive sort. He seems to only be interested in gold."}
   {:id "Beetle Chow"
    :description "Feed the forest creatures."
    :price 5000
    :result "The beetle population has rebounded!"}
   {:id "Practice Haggling"
    :description "Spend some time practicing your pitch in a nearby pool."
    :result "You can really sell em!"
    :price 8000
    :on-buy improve-haggle}
   {:id "Bad demanor"
    :description "Find something to deter those heroes from unloading all of their monster parts."
    :result "You ate some garlic"
    :price 9999
    :on-buy less-junk}
   {:id "More stuff"
    :description "Develop a monster breeding program. More dead heroes, more loot for the rest."
    :result "Monsters are stronger then ever!"
    :price 8000
    :on-buy more-drops}
   {:id "Even bigger pack"
    :description "Get an even bigger pack"
    :price 5000
    :result "The meta-peddler has sold you an even bigger pack!"
    :on-buy expand-peddlers}
   {:id "Larger bags"
    :description "Practice your pitch to get larger bags from others"
    :price 7000
    :result "Everyone will give you bigger bags now!"
    :on-buy expand-base-bag-size}
   {:id "Attract heroes"
    :description "There aren't enough heroes in the forest..."
    :price 9000
    :result "More heroes are coming, with more junk!"
    :on-buy more-junk}
   {:id "Giants In The Forest"
    :price 9000
    :description "Plant giant bean stocks to toughen up the forest population."
    :result "The magic beans are working! Giants have returned to the forest"}
   {:id "Monster training regiment"
    :description "Develop a monster education program... Maybe some of them will pick up smithing!"
    :result "Monsters are killing heroes left and right!"
    :price 10000
    :on-buy more-drops}
   {:id "Practice Haggling More"
    :description "Spend a couple days practicing your pitch in a nearby pool."
    :result "You got lost in the forest a couple times, but the trees love you!"
    :price 10000
    :on-buy improve-haggle}
   {:id "Still bigger pack"
    :description "Get an yet larger pack"
    :price 9000
    :result "The meta-peddler has sold you a yet larger pack!"
    :on-buy expand-peddlers}
   {:id "Junk drive"
    :description "This forest is starting to smell. Encourage heroes to clean up!"
    :price 9000
    :result "It's working! They're picking up the junk and bringing it to you!"
    :on-buy more-junk}
   {:id "Penultimate pack"
    :description "Get a significantly larger pack"
    :price 10000
    :result "The meta-peddler has sold you a significantly larger pack!"
    :on-buy (comp expand-peddlers expand-peddlers)}
   {:id "Sell icecubes to eskimos"
    :description "Take a trip to the arctic to improve your pitch."
    :result "The cold was really good for your skin! Also you learned a thing or two about selling airline tickets."
    :price 20000
    :on-buy improve-haggle}
   {:id "Learn Tailoring"
    :description "Learn Tailoring"
    :price 12000
    :result "You've discovered an ingenious way to sew pockets into new bags! All right!"
    :on-buy expand-base-bag-size}
   {:id "Ultimate pack"
    :description "Get an incredibly large pack"
    :price 16000
    :result "The meta-peddler has sold you an incredibly large pack!"
    :on-buy (comp expand-peddlers expand-peddlers)}
   ])

(defn has-upgrade [id]
  (contains? (:upgrades @app-state) id))

(defn next-upgrade []
  (get upgrades (count (:upgrades @app-state))))

(defn upgrade []
  (let [next (next-upgrade)]
    (if (> (:gold @app-state) (:price next))
      (do
        (log (:result next))
        (if-not (nil? (:on-buy next)) ((:on-buy next)))
        (apply-state #(-> %
                          (assoc :gold (- (:gold @app-state) (:price next)))
                          (update :upgrades (fn [u] (conj u (:id next))))))))))

(defn bag-price []
  (* (:purchased-bag-count @app-state) 1000))

(declare make-a-page!)
(defn buy-bag []
  (if (> (:gold @app-state) (bag-price))
    (do
      (apply-state #(assoc % :gold (- (:gold %) (bag-price))))
      (apply-state #(update % :purchased-bag-count inc))
      (make-a-page! (str "Metapeddler's " (:purchased-bag-count @app-state))
                    (+ 4 (:new-bag-size-x @app-state))
                    (:new-bag-size-y @app-state)))))

(defn element-style [grid-px]
  {:width (px grid-px)
   :height (px grid-px)
   :margin "0px"
   :line-height "0px"
   :display "inline-block"
   :overflow "hidden"
   :padding-top "3px"
   :color fg-color
   :font-size "8px"
   :border (str "1px solid rgba(0,0,0,0.6)") })

(defn handle-grid-element [event]
  (released-in-grid (element (:held @app-state)) (element (.-id (.-target event)))))

(defn grid-element [info x y]
  [:div
   (s ((memoize element-style) (:grid-px info))
      {:x x
       :y y
       :id (str (:name info) "-" x "-" y)
       :page (:name info)
       :on-mouseup handle-grid-element})
   (if DEBUG (str x "-" y) " ")])

(defn grid-row [info row-idx]
  [:div
   (s {:width "100%"
       :height (px (:grid-px info))
       :line-height "0px"
       :white-space "nowrap"})
   (map #(grid-element info % row-idx) (range (:width info)))])

(def stupid-border
  {:border-style "solid"
   :border-color "rgba(0,0,0,0)"
   :border-image (clojure.string/join " " [(css-url "border.png") 258 "stretch"])
   :border-width (px 8)})

(defn build-description [info]
  [:div
   (s {
       :position "absolute"
       :background-color bg-color
       :color fg-color
       :z-index "500"
       :border "1px solid black"
       :width "12vw"}
      {:class "hover-description"
       :id (str (:id info) "-description")})
   (:description info)])

(defn escape [string]
  (clojure.string/replace string " " "-"))

(defn generate-border [info]
  (let [has? #(and ((comp not nil?) %) ((comp not empty?) %))
        prefix (:prefix info)
        suffix (:suffix info)]
  (cond
    (and (has? prefix) (has? suffix)) "2px solid gold"
    (has? prefix) "2px solid #3366EE"
    (has? suffix) "2px solid #33EE66"
    :else "")))

(declare check-price)
(defn build-shit [info]
  (if-let [parent (element (str (:page info) "-grid"))]
    [:div
     [:div (s
            (merge (if (= (:type info) MONSTER) {:border "1px solid gray"} {})
                   {:width  (px (* (:size-x info) (.getAttribute parent "grid-px")))
                    :height (px (* (:size-y info) (.getAttribute parent "grid-px")))
                    :background-image (str "url(/images/" (escape (:root info)) ".svg)")
                    :background-size "contain"
                    :background-position "center"
                    :background-repeat "no-repeat"
                    :border (generate-border info)
                    :position "absolute"
                    :left "5vw"
                    :z-index "2"
                    :overflow "visible"
                    :bottom "0"})
            {:id (:id info)
             :x (:mount-x info)
             :y (:mount-y info)
             :size-x (:size-x info)
             :size-y (:size-y info)
             :on-touchstart #(if (valid-placement info
                                                   {:page "hand" :mount-x 0 :mount-y 0}
                                                   (get-page "hand"))
                                (do (update-mount (:id info) "hand" 0 0)
                                    (check-price))
                                (let [valid (first (find-valid-placement info))]
                                  (do (update-mount (:id info)
                                                    (:page valid)
                                                    (:x valid)
                                                    (:y valid))
                                      (check-price))))
                              :on-mousedown #(apply-state hold (-> % .-target .-id))})]
      (build-description info)]))

(defonce shit (hipo/create [:div (map build-shit (get-shits))]))
(defn draw-shit []
  (hipo/reconciliate! shit [:div (map build-shit (get-shits))]))

(defn build-debug []
  [:div (s {:color fg-color})
   [:div (s {:color fg-color})
    (:screen-x @app-state) "," (:screen-y @app-state)]
   (str
    (:mouse-x @app-state) "," (:mouse-y @app-state)
    " - " (:held @app-state) " - $" (:offered @app-state))])

(defonce debug (hipo/create (build-debug)))
(defn draw-debug []
  (if DEBUG (hipo/reconciliate! debug (build-debug))))

;; ui element builders

(defn build-grid [{:keys [height width grid-px name]} ]
  ;; Build us a grid of elements
  [:div
   (s (merge stupid-border
             {:width (px (+ 16 (* width grid-px)))
              :height (px (+ 16 (* height grid-px)))
              :z-index 1
              :background-color bg-color
              :bottom 0
              :flex-shrink 0
              :left 0})
      {:id (str name "-grid")
       :grid-px grid-px
       :size-x width
       :size-y height})
   (map (partial grid-row {:height height :width width :grid-px grid-px :name name} ) (range height))])

(defn build-hand []
  ;; Build us a grid of elements
  [:div
   (s {:height "90%"
       :display "flex"
       :align-items "center"
       :justify-content "space-around"
       :flex-flow "column nowrap"
       :overflow "visible"
       :width "50%"})
   (build-grid {:height 5 :width 3 :grid-px (/ (min (:screen-y @app-state) (:screen-x @app-state)) 20) :name "hand"})
   [:button
    (s {:padding (px 0)
        :height "20%"
        :width "100%"
        :font-size "inherit"
        :flex-shrink 0}
       {:id "sell-button"
        :on-click #(sell)})
    (if (nil? (:offered @app-state)) "Skip" "Sell")]])

(defn build-gold []
  [:div (s {:display "flex"
            :align-items "center"
            :width  "100%"
            :height "10%"
            :padding (padding)
            :background-color bg-color})
   [:h1 (s {:font-size "100%"
            :height "2.5vmin"
            :overflow "visible"
            :color fg-color})
    "Gold:"]
   [:p (s {:font-size "150%"
           :height "2.5vmin"
           :overflow "visible"
           :color "gold"})
    (str (:gold @app-state))]])

(defn build-total []
  [:div (s {:display "flex"
            :align-items "center"
            :width  "100%"
            :height "10%"
            :padding (padding)
            :border-bottom inv-border
            :background-color bg-color})
   [:h1 (s {:font-size "100%"
            :height "2.5vmin"
            :overflow "visible"
            :color fg-color})
    "Capital:"]
   [:p (s {:font-size "100%"
           :height "2.5vmin"
           :overflow "visible"
           :color "gold"})
    (str (:total @app-state))]])

(defn build-upgrades []
  (let [next (next-upgrade)]
    [:div (s {:display "flex"
              :justify-content "space-around"
              :align-items "center"
              :flex-direction "column"
              :height "25%"
              :width  "100%"
              :padding (padding)
              :background-color bg-color})
     [:div (s {:flex-grow 1
               :color fg-color
               :width "100%"}) (:description next)]
     [:button
      (s {:padding (px 0)
          :font-size "inherit"
          :width "100%"
          :margin 0
          :flex-shrink 0}
         {:id "upgrade-button"
          :on-click #(upgrade)})
      (str "Upgrade: "(:price next) " gp")]
     (if (has-upgrade "Buy Bags")
       [:button
        (s {:padding (px 0)
            :font-size "inherit"
            :width "100%"
            :margin 0
            :flex-shrink 0}
           {:id "buy-bag-button"
            :on-click #(buy-bag)})
        (str "Bag: " (bag-price) "gp")])
     ]))

(defn build-hero-b [url]
  [:div (s {
            :position "absolute"
            :z-index "0"
            :background-color "none"
            :background-repeat "no-repeat"
            :background-size  "contain"
            :background-position "center"
            :background-image (css-url url)}
           {:class "hero-here"
            :id    "hero-b"})

   nil])

(defn build-hero-a [url]
  [:div (s {
            :position "absolute"
            :z-index "0"
            :background-color "none"
            :background-repeat "no-repeat"
            :background-size  "contain"
            :background-position "center"
            :background-image (css-url url)}
           {:class "hero-here hero-enter"
            :id    "hero-a"})

   nil])

(defn build-peddler [url]
  [:div (s {:width   "20%"
            :height  "48%"
            :left    "50%"
            :position "absolute"
            :bottom   "4%"
            :z-index "0.5"
            :background-color "none"
            :background-repeat "no-repeat"
            :background-size  "contain"
            :background-position "center"
            :background-image (css-url url)})
   nil])

(defn build-dialog []
  (into [] (concat
            [:div (s {:width "100%"
                      :height "35%"
                      :border-left inv-border
                      :border-right inv-border
                      :padding (padding)
                      :flex-shrink 1
                      :background-color bg-color
                      :color fg-color
                      :overflow-y "hidden"
                      :transform "rotate(-180deg)"})]
            (reverse (:dialog @app-state)))))

(defn build-log []
  (into [] (concat
            [:div (s {
                      :padding (padding)
                      :flex-grow 1
                      :background-color bg-color
                      :color fg-color
                      :border-right inv-border
                      :transform "rotate(-180deg)"}
                     {:id "logs-list"})]
            (map (partial conj
                          [:p (s {:margin-top (margin 2)
                                  :line-height "110%"
                                  :overflow "visible"
                                  :font-size "80%"
                                  :transform "rotate(-180deg)"})])
                 (:logs @app-state)))))

(defn build-details [thingy]
  [:div (s {:width "100%"
            :padding (padding)
            :background-color bg-color
            :color fg-color
            :font-size "80%"
            :border-top inv-border
            :height "15%"})
   (str thingy)])

(defn stars-for [rank]
  (cond
    (= rank 0)  0
    (= rank 1)  3.4
    (< rank 5)  2.4
    (< rank 10) 1.4
    :else 0))

(defn calculate-stars [offering]
  (if (nil? offering) 0
      (js/Math.floor (+ (stars-for (:root-rank offering))
                        (stars-for (:prefix-rank offering))
                        (stars-for (:suffix-rank offering))))))

(defn build-item-description [offering]
  [:div (s {:width "100%"
            :background-color bg-color
            :color fg-color
            :flex-shrink "0"
            :display "flex"
            :flex-direction "column"
            :padding (padding)
            :border-top inv-border
            :height "15%"})
   (let [stars (calculate-stars offering)]
     [:div (s {:flex-shrink 0 :height "auto"})
      (clojure.string/join (concat (repeat stars "*") (repeat (- 10 stars) "-")))])
   (if (:gold offering) [:h1  (s {:color "gold"
                                  :flex-grow 1
                                  :width "100%"
                                  :padding-top (padding)
                                  :text-align "center"})
                         (str (:gold offering) " gp")])])

(defn build-dungeon []
  [:div (s {:width   "100%"
            :height  "65%"
            :display "flex"
            :z-index "0"
            :flex-shrink 0
            :position "relative"
            :background-color "#D0D0D0"
            :background-image "url(\"/images/Background.png\")"
            :background-position "left center"
            :background-size "cover"
            :flex-flow "row nowrap"
            :justify-content "space-between"})
   (build-hero-a (:hero-portrait-a @app-state))
   (build-hero-b (:hero-portrait-b @app-state))
   (build-peddler (:peddler-portrait @app-state))
   [:div (s {:flex-shrink 0
             :display "flex"
             :flex-flow "column nowrap"
             :align-items "center"
             :justify-content "flex-start"
             :width "20%"
             })
    (build-gold)
    (build-details (hero-name (:hero @app-state)))
    (build-item-description (:offered @app-state))
    (build-hand)]
   (build-dialog)
   [:div (s {:flex-shrink 0
             :width "20%"
             })
    (build-total)
    (build-upgrades)
    ]])

(defn build-bag [name width max-height]
  (let [page (get-in @app-state [:pages name])]
    (build-grid {:height (:size-y page) :width (:size-x page) :grid-px (min (/ width (:size-x page)) (/ max-height (:size-y page))) :name name})))

(declare switch-to-page!)
(defn build-tabs [pages]
  (into []
        (concat [:div (s {:min-width "10%"
                          :max-width "30%"
                          :overflow-x "visible"
                          :overflow-y "auto"
                          :display "flex"
                         ;; :border-bottom inv-border
                          :flex-flow "column nowrap"
                          :transform "rotate(-180deg)"
                          :z-index 2 })]
                [[:div
                  (s {:min-height (px 10)
                      :height "auto"
                      :background-color "rgba(0,0,0,0.4)"
                      :border-left inv-border
                      :flex-grow 1})
                  nil]]
                (interleave
                 (repeat [:div
                              (s {:min-height (px 10)
                                  :height "auto"
                                  :flex-shrink 0
                                  :background-color "rgba(0,0,0,0.4)"
                                  :border-left inv-border
                                  :flex-grow 0})
                              nil])
                 (map #(vector
                        :div (s (merge
                                 {:height "4vmin"
                                  :background-color bg-color
                                  :color fg-color
                                  :display "flex"
                                  :transform "rotate(-180deg)"
                                  :flex-shrink 0
                                  :padding (padding)
                                  :flex-grow 0
                                  :z-index 2}
                                 (if (= % (first (:visible-pages @app-state)))
                                   {:border-top inv-border
                                    :border-bottom inv-border}
                                   {:border-right inv-border})
                                 (if (= % "Peddler's")
                                   {:border-top "none"}))
                                {:on-click (partial switch-to-page! %)
                                 :on-mouseup (partial released-in-tab %)})
                        %)
                      (reverse pages))))))

(defn build-bags []
  [:div (s {:width "100%"
            :display "flex"
            :position "relative"
            :background-color "#002c00"
            :z-index "0"
            :flex-flow "row nowrap"
            :flex-grow 1
            :justify-content "space-between"})
   (build-tabs (:page-order @app-state))
   [:div (s {
             :min-width "60%"
             :flex-shrink 1
             :display "flex"
             :align-items "center"
             :justify-content "center"
             :background-color bg-color}
            {:id "main-bag-parent"})
    (let [parent (.getBoundingClientRect (or (element "main-bag-parent") (element "app")))
          width  (* (.-width parent) 0.94)
          height (* (.-height parent) 0.8)]
      (build-bag  (first (:visible-pages @app-state)) width height))]
   (build-log)])

;; ui elements

(defonce dungeon          (hipo/create (build-dungeon)))
(defonce bags             (hipo/create (build-bags)))

;; ui element drawers

(defn draw-dungeon []
  (hipo/reconciliate! dungeon (build-dungeon)))

(defn draw-bags []
  (hipo/reconciliate! bags (build-bags)))

;; Other stuff
(defn position [id left top]
  (let [target (element id)]
    (do
      (set! (-> target .-style .-visibility) "")
      (set! (-> target .-style .-opacity) "")
      (set! (-> target .-style .-bottom) "")
      (set! (-> target .-style .-right) "")
      (set! (-> target .-style .-left) (px left))
      (set! (-> target .-style .-top) (px (+ top (.-scrollY js/window) ))))))

(defn show [id]
  (let [target (element id)]
    (do
      (set! (-> target .-style .-visibility) "")
      (set! (-> target .-style .-opacity) ""))))

(defn hide [id]
  (let [target (element id)]
    (do
      (set! (-> target .-style .-visibility) "hidden")
      (set! (-> target .-style .-opacity) "0"))))

(defn place-on-mount [shit]
  (let [mount (element (mount-key shit) )]
    (if mount
      (position (:id shit)
                (-> mount .getBoundingClientRect .-left)
                (-> mount .getBoundingClientRect .-top)))))

(defn place-description [shit]
  (let [shit-element (element (:id shit))]
    (if shit-element
      (position (str (:id shit) "-description")
                (-> shit-element .getBoundingClientRect .-right)
                (-> shit-element .getBoundingClientRect .-top)))))

(defn hide-description [shit]
  (hide (str (:id shit) "-description")))

(defn place-held-item []
  (let [shits (get-shits)]
  (do
    (doall (map place-on-mount shits))
    (if (nil? (:held @app-state))
      (do (doall (map restore-z (map #(:id %) shits)))
        (doall (map place-description shits))))
    (if (:held @app-state)
      (let [target (element (:held @app-state))]
        (if target
          (do (position (:held @app-state)
                        (+ 10 (:mouse-x @app-state))
                        (+ 10 (:mouse-y @app-state)))
              (set-top-z (:held @app-state))
              (doall (map hide-description shits)))))))))

(def colors ["red" "blue" "green"
             "teal" "yellow" "purple"
             "orange" "salmon"
             "pink" "gold" "violet"])

(defn make-a-page!
  ([name size-x size-y]
   (swap! app-state #(-> %
                         (assoc-in [:pages name] {:size-x size-x :size-y size-y})
                         (update :page-order conj name))))
  ([name]
   (make-a-page! name (:new-bag-size-x @app-state) (:new-bag-size-y @app-state))))

(defn buy [id shit valid]
  (if (> (calculate-value shit) (:gold @app-state))
    (say (str "Ouch, you can't afford my " (shit-name shit)))
    (do
      (log (str "Bought " (shit-name shit) " " (calculate-value shit) " gp."))
      (apply-state #(assoc % :gold (- (:gold @app-state) (calculate-value shit))))
      (apply-state #(assoc-in % [:pages (:page valid) :shits id]
                              (merge shit
                                     {:mount-x (:x valid)
                                      :mount-y (:y valid)
                                      :color (always-pick colors)
                                      :description (shit-name shit)}))))))

(defn take-a-shit []
  (let [id (str (random-uuid))
        shit (generate-shit)
        valid (first (find-valid-placement (merge shit {:id id})))]
    (if (nil? valid)
      (do (make-a-page! (str (:first-name (:hero @app-state)) "'s"))
          (say "You're out of bags! Here, have one of mine."))
      (buy id shit valid))))

(defn take-a-bit []
  (let [id (str (random-uuid))
        bit (generate-monster-bit)
        valid (first (find-valid-placement (merge bit {:id id})))]
    (if (nil? valid)
      (do (make-a-page! (str (:first-name (:hero @app-state)) "'s" ))
          (say "You're out of bags! Here, have one of mine."))
      (buy id bit valid))))

(defn switch-to-page! [name]
  (swap! app-state assoc-in [:visible-pages 0] name))

(defn draw []
  (.appendChild body-element shit)
  (.appendChild (element "app") dungeon)
  (.appendChild (element "app") bags)
  (apply-state #(assoc % :hero (generate-hero)))
  (new-hero-appears (:hero @app-state))
  (doall (repeatedly 10 take-a-shit))
  (hide "cover")
  (if DEBUG (.appendChild body-element debug)))

(defn redraw [key atom old new]
  (let [changed #(not= (% old) (% new))]
    (draw-debug)
    (if (or (changed :hero)
            (changed :gold)
            (changed :hero-portrait-a)
            (changed :hero-portrait-b)
            (changed :offered)
            (changed :dialog)
            (changed :purchased-bag-count)
            (changed :upgrades)
            (changed :screen-x)
            (changed :screen-y))
      (draw-dungeon))
    (when (or (changed :pages)
              (changed :visible-pages)
              (changed :logs)
              (changed :upgrades)
              (changed :screen-x)
              (changed :screen-y))
      (draw-bags))
    (when (or (changed get-shits-in)
              (changed :upgrades)
              (changed :screen-x)
              (changed :screen-y))
      (draw-shit))
    (place-held-item)))

(defn on-js-reload []
  (draw-debug)
  (draw-shit)
  (draw-bags)
  (draw-dungeon)
  (place-held-item))

(defonce watch-state
  (add-watch app-state :main redraw))

(defonce startup
  (let [app (.getElementById js/document "app")]
    (do
      (set! (.-innerHTML app) "")
      (draw))))
